package ThreadMail;

import ThreadMail.TaskMail;

public class WorkerMail implements Runnable {

	  private final TaskMail task;
	  public WorkerMail(final TaskMail task) {
	    this.task = task;
	  }

	  @Override
	  public void run() {
	    System.out.println(String.format("%s API MAIL %s", Thread.currentThread().getName(),
	        task.toString()));
	    
	    task.setEstado(task.toString());
	    try {
	      Thread.sleep(task.getTimeMs());
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
	  }
}