package apisMail;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import Servicio.IservicioMail;
import Model.JClima;
import Model.JTiempo;

public class ApiMailSmtp implements IservicioMail {
	
	public static final String mail_smtp_host = "smtp.gmail.com";
	public static final String mail_smtp_starttls_enable = "true";
	public static final String mail_smtp_user = "maxh@seincomp.com.ar";
	public static final String mail_smtp_password = "11aa22bb";
	public static final String mail_smtp_port = "587";
	public static final String mail_smtp_auth = "true";
	public static final String mail_smtp_transport = "smtp";
	String urlpagina = "www.google.com";

	  public String getUrl()
	  {		  
		  return urlpagina;
	  }

	  
  public String EnviarMail(JClima clima) {

		final String username = mail_smtp_user;
        final String password = mail_smtp_password;

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
          new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
          });

        try {

            String ClimaString = " Clima " +
     	 	       " <p>Direccion</p>" + clima.getAddress().toString() + " <br/> " +
     	 	       " <p>Condición</p>" + clima.getConditions().toString()  + " <br/> " +
     	 	       " <p>Humedad</p>" + clima.getHumidity().toString()  + " <br/> " +
     	 	       " <p>Maxima temperatura</p>" + clima.getMaxt().toString()  + " <br/> " +
     	 	       " <p>Precipitación</p>" + clima.getPrecip().toString()  + " <br/> " +
     	 	       " <p>Temperatura</p>" +  clima.getTemp().toString() + " <br/>";
     		        
            
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress("max1992@hotmail.com.ar"));
            message.setSubject("Email enviado desde SMTP");
            message.setText(ClimaString);

            Transport.send(message);

            System.out.println("Enviado");
            return "Enviado";

        }catch (Exception e) {
			e.printStackTrace();
			return "Error";
	}
  }
	
}
