package Servicio;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.client.ClientProtocolException;

import com.mashape.unirest.http.exceptions.UnirestException;

import Model.JClima;
import Model.JTiempo;

public interface IservicioMail {
	public String getUrl();
	public String EnviarMail(JClima clima) throws UnirestException;

}
