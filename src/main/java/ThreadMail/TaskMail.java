package ThreadMail;

import java.util.concurrent.atomic.AtomicInteger;

public abstract class TaskMail {

	  private static final AtomicInteger ID_GENERATOR = new AtomicInteger();

	  private final int id;
	  private final int timeMs;

	private String estado;

	  public TaskMail(final int timeMs) {
	    this.id = ID_GENERATOR.incrementAndGet();
	    this.timeMs = timeMs;
	  }

	  public int getId() {
	    return id;
	  }

	  public int getTimeMs() {
		    return timeMs;
		  }

	  
	  public String getEstado() {
		    return estado;
		  }
	  
      public  void setEstado (String estado)
      {
    	this.estado=estado;
      } 
      
	  @Override
	  public String toString() {
	    return String.format("id=%d timeMs=%d", id, timeMs);
	  }
	}