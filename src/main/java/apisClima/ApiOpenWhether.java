package apisClima;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import Servicio.Iservicio;
import Model.JTiempo;

public class ApiOpenWhether implements Iservicio{
	
     	String urlpagina=" www.openweathermap.org";
	    String API_KEY = "988ebbfc0bc3f8b2039b9ec2f4f53b33";

	  public String getUrl()
	  {
		  
		  return urlpagina;
	  }

	 public static Map<String,Object> jsonToMap(String str){
	        Map<String,Object> map = new Gson().fromJson(str,new 
	    TypeToken<HashMap<String,Object>> () {}.getType());
	        return map;
	 }
	    
	public String getTiempo(JTiempo model) {
	    StringBuilder result = new StringBuilder();
	    String LOCATION = model.getPais() + "," + model.getProvincia() + ", BUE";
	    String urlString = "http://api.openweathermap.org/data/2.5/weather?q=" + LOCATION + "&appid=" + API_KEY + "&units=metric";
	    
		  try{		    
		        URL url = new URL(urlString);
		        URLConnection conn = url.openConnection();
		        BufferedReader rd = new BufferedReader(new InputStreamReader (conn.getInputStream()));
		        String line;
		        
		        while ((line = rd.readLine()) != null){
		            result.append(line);
		        }

		        rd.close();
            
		    }catch (IOException e){
		        System.out.println(e.getMessage());
		    }
		  
		   return result.toString();
           

	}
}
