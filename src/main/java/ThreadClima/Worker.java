package ThreadClima;

public class Worker implements Runnable {

	  private final Task task;
	  public Worker(final Task task) {
	    this.task = task;
	  }

	  @Override
	  public void run() {
	    System.out.println(String.format("%s API CLIMA %s", Thread.currentThread().getName(),
	        task.toString()));
	    
	    task.setEstado(task.toString());
	    try {
	      Thread.sleep(task.getTimeMs());
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
	  }
}