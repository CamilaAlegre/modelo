package Servicio;

import java.io.IOException;

public class Servicio {
	
	public static String DevolverStatusPage(String url) throws IOException, InterruptedException {
		Runtime runtime = Runtime.getRuntime();
		Process proc = runtime.exec("ping "+url); 

		int mPingResult = proc .waitFor();
		if(mPingResult == 0){
		       return "Con conexion";
		}else{
		       return "Sin conexion";
		}
	}
}
