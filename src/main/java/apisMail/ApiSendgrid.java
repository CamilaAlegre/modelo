package apisMail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import com.sendgrid.*;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.*;

import Servicio.IservicioMail;
import Model.JClima;
import Model.JTiempo;

public class ApiSendgrid implements IservicioMail {

	String urlpagina = "www.Sengrid.com";
	
	  public String getUrl()
	  {
		  
		  return urlpagina;
	  }

	  
	public String EnviarMail(JClima clima) {
		 try {
		        com.sendgrid.helpers.mail.objects.Email from = new com.sendgrid.helpers.mail.objects.Email("maxh@seincomp.com.ar");
		        com.sendgrid.helpers.mail.objects.Email to = new com.sendgrid.helpers.mail.objects.Email("maxh@seincomp.com.ar"); 

		        String ClimaString = " Clima " +
	 	       " <p>Direccion</p>" + clima.getAddress().toString() + " <br/> " +
	 	       " <p>Condicin</p>" + clima.getConditions().toString()  + " <br/> " +
	 	       " <p>Humedad</p>" + clima.getHumidity().toString()  + " <br/> " +
	 	       " <p>Maxima temperatura</p>" + clima.getMaxt().toString()  + " <br/> " +
	 	       " <p>Precipitacin</p>" + clima.getPrecip().toString()  + " <br/> " +
	 	       " <p>Temperatura</p>" +  clima.getTemp().toString() + " <br/>";
		        
		        String subject = "Email enviado desde SENDGRID.";
		        Content content = new Content("text/html", "<em>Clima hoy</em> "+ ClimaString +" <strong>Java</strong>");
		        
		        Mail mail = new Mail(from,subject,to,content);

		        SendGrid sg = new SendGrid("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");
		        Request request = new Request();
		        request.addHeader("Access-Control-Allow-Origin", "*");
		        request.setMethod(Method.POST);
		        request.setEndpoint("mail/send");
		        request.setBody(mail.build());

		        Response response = sg.api(request);

		        System.out.println(response.getStatusCode());
		        System.out.println(response.getHeaders());
		        System.out.println(response.getBody());
		        
		      } catch (IOException ex) {
		    	   System.out.println(ex.toString());
		      }
		 
		return "CORRECTO";
	}
	
}
