package Servicio;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.client.ClientProtocolException;

import Model.JTiempo;

public interface Iservicio {
	public String getUrl();
	public String getTiempo(JTiempo model) throws URISyntaxException, ClientProtocolException, IOException;
}
