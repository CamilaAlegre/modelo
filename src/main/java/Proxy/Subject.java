package Proxy;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.client.ClientProtocolException;

import Model.JTiempo;

public interface Subject {

	
	
	
	public String getTiempo(JTiempo model) throws ClientProtocolException, URISyntaxException, IOException ;
}
