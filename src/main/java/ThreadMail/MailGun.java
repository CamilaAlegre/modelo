package ThreadMail;

import java.io.IOException;

import ThreadClima.Task;
import Servicio.Servicio;

public class MailGun extends TaskMail {


	private static final int TIME_PER_CUP = 100;

	  public MailGun(int numCups) {
	    super(numCups * TIME_PER_CUP);
	  }

	  @Override
	  public String toString() {
		 Servicio servicio = new Servicio();
		 String statud = "";
		 try {
			statud = servicio.DevolverStatusPage("www.mailgun.com");
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		 
	    return String.format("%s,%s", this.getClass().getSimpleName(), statud);
	  }
	  
	  
	}