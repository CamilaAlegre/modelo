package AdministrarJson;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import Model.JClima;

public class DescomponerJSONClima {

    public JClima JSonClima(String JsonClima) {
    	 JClima clima = new JClima();
    	 
		 if(JsonClima.contains("location")) {
			 clima =  JsonClimaVisualCrossing(JsonClima);
		 }else {
			 clima =  JsonClimaOpenWhater(JsonClima);
		 }
		 
		 return clima;
    }
	
	public JClima JsonClimaVisualCrossing(String JsonClima) {
		 JClima clima = new JClima();
		
		 JSONObject jsonWeatherForecast = null;
		 jsonWeatherForecast = new JSONObject(JsonClima);
		 
		 JSONObject location=jsonWeatherForecast.getJSONObject("location");
		 
		 JSONArray values=location.getJSONArray("values");
		 JSONObject forecastValue = values.getJSONObject(1);
		   double humidity=forecastValue.getDouble("humidity");
		   double maxt=forecastValue.getDouble("maxt");
		   double precip=forecastValue.getDouble("precip");
		   double temp=forecastValue.getDouble("temp");
		   
		 clima.setAddress(location.getString("address")); 		 
		 clima.setConditions(forecastValue.getString("conditions"));
		 clima.setHumidity(String.valueOf(humidity));
		 clima.setMaxt(String.valueOf(maxt));
		 clima.setPrecip(String.valueOf(precip));
		 clima.setTemp(String.valueOf(temp));

		return clima;
	}
	
	public JClima JsonClimaOpenWhater(String JsonClima) {
		 JClima clima = new JClima();
		
		 JSONObject jsonWeatherForecast = null;
		 jsonWeatherForecast = new JSONObject(JsonClima);
		 
		 JSONArray weather=jsonWeatherForecast.getJSONArray("weather");
		 JSONObject forecastValue = weather.getJSONObject(0);
		 
		 Map<String, Object > respMap = jsonToMap (JsonClima.toString());
	     Map<String, Object > mainMap = jsonToMap (respMap.get("main").toString());
	     
	     clima.setAddress(respMap.get("name").toString());
		 clima.setConditions(forecastValue.getString("description"));
		 clima.setHumidity(mainMap.get("humidity").toString());
		 clima.setMaxt(mainMap.get("temp_max").toString());
		 clima.setPrecip(mainMap.get("pressure").toString());
		 clima.setTemp(mainMap.get("temp").toString());

		return clima;
	}
	
	 public static Map<String,Object> jsonToMap(String str){
	        Map<String,Object> map = new Gson().fromJson(str,new 
	    TypeToken<HashMap<String,Object>> () {}.getType());
	        return map;
	 }
	    
	
}
