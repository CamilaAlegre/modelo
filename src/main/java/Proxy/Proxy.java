package Proxy;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;

import org.apache.http.client.ClientProtocolException;

import Cache.SimpleCacheManager;
import Servicio.Iservicio;
import Model.JTiempo;

public class Proxy implements Subject{

	HashMap<String, String> map  = new HashMap<>(); 
	public String getTiempo(JTiempo model) throws ClientProtocolException, URISyntaxException, IOException {


		String clave =model.getPais()+","+model.getProvincia()+","+model.getLocalidad();
        String Tiempo = "";
        
        if (SimpleCacheManager.getInstance().containsKey(clave)) { 
        	Tiempo = SimpleCacheManager.getInstance().get(clave).toString();
        	
        } else {
        	RealSubject app = new RealSubject();
    		Tiempo =app.getTiempo(model);		
        	SimpleCacheManager.getInstance().put(clave, Tiempo);
        }
        
        return Tiempo;
	}
}
