package ThreadClima;

import java.io.IOException;

import Servicio.Servicio;

public class OpenWhether extends Task {


	private static final int TIME_PER_CUP = 100;

	  public OpenWhether(int numCups) {
	    super(numCups * TIME_PER_CUP);
	  }

	  @Override
	  public String toString() {
		 Servicio servicio = new Servicio();
		 String statud = "";
		 try {
			statud = servicio.DevolverStatusPage("www.openweathermap.org");
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		 
	    return String.format("%s,%s", this.getClass().getSimpleName(), statud);
	  }
	  
	  
	}