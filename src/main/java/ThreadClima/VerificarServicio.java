package ThreadClima;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import Servicio.Iservicio;
import apisClima.ApiOpenWhether;
import Model.JTiempo;

public class VerificarServicio  {

	  static public Iservicio VerificarServicio(JTiempo model)  {

	    System.out.println("Inicio");
	    
	    List<Task> tasks = new ArrayList<>();

	    tasks.add(new OpenWhether(1));
	    tasks.add(new VisualCroosing(1));

	    ExecutorService executor = Executors.newFixedThreadPool(3);

	    for (int i = 0; i < tasks.size(); i++) {
	      Runnable worker = new Worker(tasks.get(i));
	      executor.execute(worker);	
	    }
	   
	    executor.shutdown();
	    while (!executor.isTerminated()) {
	      Thread.yield();
	    }


	    for (int i = 0; i < tasks.size(); i++) {
	    	
	    	String[] parts = tasks.get(i).getEstado().split(",");              
	    	
		    if( parts[1].equals("Con conexion"))
		     {
		    	return ejemplo("Api"+parts[0]);
		     }
		    }
	    
		return null;
	  }
	  
	  
	  public static Iservicio ejemplo(String cadena) 
	  {
		  
		  try {
			return  (Iservicio) Class.forName("net.codejava.apiClima." + cadena).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  return null;
	    	
	  }
	  
	}