package ThreadMail;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import Servicio.Iservicio;
import Servicio.IservicioMail;

public class AppMail {
	
	  public IservicioMail VerificarServicio()  {

		    System.out.println("Inicio");
		    
		    List<TaskMail> tasks = new ArrayList<>();

		    tasks.add(new MailGun(1));
		    tasks.add(new Sendgrid(1));
		    tasks.add(new MailSmtp(1));
		    
		    ExecutorService executor = Executors.newFixedThreadPool(3);

		    for (int i = 0; i < tasks.size(); i++) {
		      Runnable worker = new WorkerMail(tasks.get(i));
		      executor.execute(worker);	
		    }
		   
		    executor.shutdown();
		    while (!executor.isTerminated()) {
		      Thread.yield();
		    }


		    for (int i = 0; i < tasks.size(); i++) {
		    	
		    	String[] parts = tasks.get(i).getEstado().split(",");              
		    	
			    if( parts[1].equals("Con conexion"))
			     {
			    	return ejemplo("Api"+parts[0]);
			     }
			 }
		    
			return null;
		  }
		  
		  
		  public static IservicioMail ejemplo(String cadena) 
		  {
			  
			  try {
				return  (IservicioMail) Class.forName("net.codejava.apiMail." + cadena).newInstance();
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			  return null;		    	
		  }
}
