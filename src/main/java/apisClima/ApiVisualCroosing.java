package apisClima;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.nio.charset.Charset;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import  Servicio.Iservicio;
import Model.JTiempo;

public class ApiVisualCroosing implements Iservicio {

	
	  private String urlpagina="www.visualcrossing.com";

	public String getUrl()
	  {
		  
		  return urlpagina;
	  }

	public String getTiempo(JTiempo model) throws URISyntaxException, ClientProtocolException, IOException {
		String LOCATION = model.getPais() + "," + model.getProvincia() + ", BUE";
		  String rawResult = "";
		  
		URIBuilder builder = new URIBuilder("https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/weatherdata/forecast");

		   builder.setParameter("aggregateHours", "24")
		   	.setParameter("contentType", "json")
		   	.setParameter("locationMode", "single")
		   	.setParameter("unitGroup", "metric")
		   	.setParameter("key", "1PYNQ6AWUDJE9AFERDCHJHSXK")
		   	.setParameter("locations", LOCATION);
		   JSONObject jsonWeatherForecast = null;
		   HttpGet get = new HttpGet(builder.build());
		   CloseableHttpClient httpclient = HttpClients.createDefault();
		   CloseableHttpResponse response = httpclient.execute(get);    


		   try {
		    if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
		    System.out.printf("Bad response status code:%d%n", response.getStatusLine().getStatusCode());
		    return "Error";
		    }
		    
		    HttpEntity entity = response.getEntity();
		       if (entity != null) {
		        rawResult=EntityUtils.toString(entity, Charset.forName("utf-8"));
             }
		        
		   } finally {
		    response.close();
		   }  
	   		   
		return rawResult.toString();

	}
}
