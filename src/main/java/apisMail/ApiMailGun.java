package apisMail;

import java.io.IOException;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import Servicio.Iservicio;
import Servicio.IservicioMail;
import Model.JClima;
import Model.JTiempo;

public class ApiMailGun implements IservicioMail  {
	 private static final String YOUR_DOMAIN_NAME = "sandbox0387f10319f04f00aef52009398e57d7.mailgun.org";
	private static final String API_KEY = "0786e623d3ddcc79bef40860eedea524-cb3791c4-2763dd15";

  	String urlpagina = "www.MailGun.com";
  	
	  public String getUrl()
	  {	  
		  return urlpagina;
	  }
		
	
	public String EnviarMail(JClima clima) throws UnirestException {
		
	     String ClimaString = " Clima " +
		 	       " <p>Direccion</p>" + clima.getAddress().toString() + " <br/> " +
		 	       " <p>Condición</p>" + clima.getConditions().toString()  + " <br/> " +
		 	       " <p>Humedad</p>" + clima.getHumidity().toString()  + " <br/> " +
		 	       " <p>Maxima temperatura</p>" + clima.getMaxt().toString()  + " <br/> " +
		 	       " <p>Precipitación</p>" + clima.getPrecip().toString()  + " <br/> " +
		 	       " <p>Temperatura</p>" +  clima.getTemp().toString() + " <br/>";
			        
	     
		 HttpResponse<com.mashape.unirest.http.JsonNode> request = Unirest.post("https://api.mailgun.net/v3/" + YOUR_DOMAIN_NAME + "/messages")
		            .basicAuth("api", API_KEY)
		            .field("from", "maxh@seincomp.com.ar")
		            .field("to", "maxh@seincomp.com.ar")
		            .field("subject", "Email enviado desde MAILGUN")
		            .field("text", ClimaString)
		            .asJson();
		        
		        System.out.print("bien");
		        System.out.print(request.getBody().toString());
		        
		        return request.getBody().toString();
	}
	
}
