package Model;

import java.sql.Date;

public class JTiempo {

	private String Pais;
	private String Provincia;
	private String Localidad;
	private String Email;
	private String Respuesta;
	
	//Clima
	private String address;
	private String temp;
	private String maxt;
	private String precip;
	private String humidity;
	private String conditions;
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTemp() {
		return temp;
	}
	public void setTemp(String temp) {
		this.temp = temp;
	}
	public String getMaxt() {
		return maxt;
	}
	public void setMaxt(String maxt) {
		this.maxt = maxt;
	}
	public String getPrecip() {
		return precip;
	}
	public void setPrecip(String precip) {
		this.precip = precip;
	}
	public String getHumidity() {
		return humidity;
	}
	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}
	public String getConditions() {
		return conditions;
	}
	public void setConditions(String conditions) {
		this.conditions = conditions;
	}
	public String getPais() {
		return Pais;
	}
	public void setPais(String pais) {
		Pais = pais;
	}
	public String getProvincia() {
		return Provincia;
	}
	public void setProvincia(String provincia) {
		Provincia = provincia;
	}
	public String getLocalidad() {
		return Localidad;
	}
	public void setLocalidad(String localidad) {
		Localidad = localidad;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getRespuesta() {
		return Respuesta;
	}
	public void setRespuesta(String respuesta) {
		Respuesta = respuesta;
	}
	@Override
	public String toString() {
		return "JTiempo [Pais=" + Pais + ", Provincia=" + Provincia + ", Localidad=" + Localidad + ", Email=" + Email
				+ ", Respuesta=" + Respuesta + ", address=" + address + ", temp=" + temp + ", maxt=" + maxt
				+ ", precip=" + precip + ", humidity=" + humidity + ", conditions=" + conditions + "]";
	}

}
