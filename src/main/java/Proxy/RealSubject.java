package Proxy;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.client.ClientProtocolException;

import ThreadClima.VerificarServicio;
import Servicio.Iservicio;
import Model.JTiempo;

public class RealSubject {

	public String getTiempo(JTiempo model) throws ClientProtocolException, URISyntaxException, IOException {
		Iservicio servicio=VerificarServicio.VerificarServicio(model);
		return servicio.getTiempo(model);
	}

	
	
}
